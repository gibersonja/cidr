package main

import (
	"fmt"
	"log"
	"os"
	"path"
	"regexp"
	"runtime"
	"strconv"
)

var verbose bool = false
var quiet bool = false

func main() {
	if len(os.Args) < 2 {
		er(fmt.Errorf("no arguments given"))
	}
	args := os.Args[1:]
	var ip uint32
	var netmask uint32
	var ips []uint32

	for n := 0; n < len(args); n++ {
		arg := args[n]

		regex := regexp.MustCompile(`-\w*h\w*`)
		if regex.MatchString(arg) || arg == "--help" {
			fmt.Print("-h | --help\tPrint this help message.\n")
			fmt.Print("-q | --quiet\tDo not print normal output\n")
            fmt.Print("-v | --verbose\tShow verbose output\n")
			fmt.Print("A.B.C.D/XY\tCIDR Address to calculate the IP range.\n")
			return
		}

		regex = regexp.MustCompile(`-\w*v\w*`)
		if regex.MatchString(arg) || arg == "--verbose" {
			verbose = true
		}

		regex = regexp.MustCompile(`-\w*q\w*`)
		if regex.MatchString(arg) || arg == "--quiet" {
			quiet = true
		}

		regex = regexp.MustCompile(`^(\d+)\.(\d+)\.(\d+)\.(\d+)\/(\d+)`)
		if regex.MatchString(arg) {

			tmp, err := strconv.Atoi(regex.FindStringSubmatch(arg)[5])
			er(err)

			for i := 0; i < tmp; i++ {
				netmask = netmask << 1
				netmask = netmask + 1
			}
			for i := 0; i < 32-tmp; i++ {
				netmask = netmask << 1
			}

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[1])
			er(err)
			ip = ip + uint32(tmp)
			ip = ip << 8

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[2])
			er(err)
			ip = ip + uint32(tmp)
			ip = ip << 8

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[3])
			er(err)
			ip = ip + uint32(tmp)
			ip = ip << 8

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[4])
			er(err)
			ip = ip + uint32(tmp)
			ips = get_cidr(ip, netmask)

		}
	}
	if verbose {
		fmt.Printf("NETWORK ADDR:\t%08b.%08b.%08b.%08b\t%d.%d.%d.%d\n",
			byte(ips[0]>>24), byte(ips[0]>>16), byte(ips[0]>>8), byte(ips[0]),
			byte(ips[0]>>24), byte(ips[0]>>16), byte(ips[0]>>8), byte(ips[0]))

		fmt.Printf("BROADCAST ADDR:\t%08b.%08b.%08b.%08b\t%d.%d.%d.%d\n",
			byte(ips[len(ips)-1]>>24), byte(ips[len(ips)-1]>>16),
			byte(ips[len(ips)-1]>>8), byte(ips[len(ips)-1]),
			byte(ips[len(ips)-1]>>24), byte(ips[len(ips)-1]>>16),
			byte(ips[len(ips)-1]>>8), byte(ips[len(ips)-1]))

		fmt.Printf("NETWORK MASK:\t%08b.%08b.%08b.%08b\t%d.%d.%d.%d\n",
			byte(netmask>>24), byte(netmask>>16), byte(netmask>>8), byte(netmask),
			byte(netmask>>24), byte(netmask>>16), byte(netmask>>8), byte(netmask))

		fmt.Printf("HOST MASK:\t%08b.%08b.%08b.%08b\t%d.%d.%d.%d\n",
			^byte(netmask>>24), ^byte(netmask>>16), ^byte(netmask>>8),
			^byte(netmask), ^byte(netmask>>24), ^byte(netmask>>16), ^byte(netmask>>8),
			^byte(netmask))

		fmt.Print("\n")
	}
	if !quiet {
		for i := ips[0]; i <= ips[1]; i++ {

			fmt.Printf("%d.%d.%d.%d\n", byte(i>>24), byte(i>>16),
				byte(i>>8), byte(i))
		}
	}
}

func get_cidr(ip uint32, netmask uint32) []uint32 {
	first_ip := ip & netmask
	last_ip := ip | ^netmask

	var data []uint32

	data = append(data, first_ip)
	data = append(data, last_ip)

	return data
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(),
			path.Base(file), line, err)
	}
}
