# cidr

## EXAMPLE
``` bash
./cidr -h
-h | --help     Print this help message.
-q | --quiet    Do not print normal output
-v | --verbose  Show verbose output
A.B.C.D/XY      CIDR Address to calculate the IP range.
```

``` bash
./cidr 1.2.3.4/29
1.2.3.0
1.2.3.1
1.2.3.2
1.2.3.3
1.2.3.4
1.2.3.5
1.2.3.6
1.2.3.7
```
``` bash
./cidr 1.2.3.4/29 -v
NETWORK ADDR:   00000001.00000010.00000011.00000000     1.2.3.0
BROADCAST ADDR: 00000001.00000010.00000011.00000111     1.2.3.7
NETWORK MASK:   11111111.11111111.11111111.11111000     255.255.255.248
HOST MASK:      00000000.00000000.00000000.00000111     0.0.0.7

1.2.3.0
1.2.3.1
1.2.3.2
1.2.3.3
1.2.3.4
1.2.3.5
1.2.3.6
1.2.3.7
```

``` bash
./cidr 1.2.3.4/29 -qv
NETWORK ADDR:   00000001.00000010.00000011.00000000     1.2.3.0
BROADCAST ADDR: 00000001.00000010.00000011.00000111     1.2.3.7
NETWORK MASK:   11111111.11111111.11111111.11111000     255.255.255.248
HOST MASK:      00000000.00000000.00000000.00000111     0.0.0.7
```
